package org.example;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.example.app.repository.NoteRepository;
import org.example.app.repository.UserRepository;
import org.example.framework.di.Container;
import org.example.framework.security.middleware.anon.AnonAuthMiddleware;
import org.example.framework.security.middleware.jsonbody.JSONBodyAuthNMiddleware;
import org.example.framework.security.postprocessor.AuditBeanPostProcessor;
import org.example.framework.security.postprocessor.HasRoleBeanPostProcessor;
import org.example.framework.server.annotation.Controller;
import org.example.framework.server.controller.ControllerRegistrar;
import org.example.framework.server.controller.handler.ReturnValueHandler;
import org.example.framework.server.controller.resolver.ArgumentResolver;
import org.example.framework.server.http.Server;
import org.example.repo.framework.RepositoryCreator;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;

import java.io.IOException;

@Slf4j

public class Main {
    public static void main(String[] args) throws IOException {
        final Container container = new Container();
        container.register("org.example");
        container.register(Gson.class);
        container.register(Argon2PasswordEncoder.class);
        container.register(JSONBodyAuthNMiddleware.class);
        container.register(new AuditBeanPostProcessor());
        container.register(new HasRoleBeanPostProcessor());

        final RepositoryCreator creator = new RepositoryCreator("default");
        creator.scan("org.example.app");
        final UserRepository userRepository = creator.provideBean(UserRepository.class);
        final NoteRepository noteRepository = creator.provideBean(NoteRepository.class);

        container.register(userRepository);
        container.register(noteRepository);
        container.wire();
        final Server server = Server.builder()
                .middleware((JSONBodyAuthNMiddleware) container.getBean(JSONBodyAuthNMiddleware.class.getName()))
                .middleware(new AnonAuthMiddleware())
                .argumentResolvers(container.getBeansByType(ArgumentResolver.class))
                .returnValueHandlers(container.getBeansByType(ReturnValueHandler.class))
                .router(new ControllerRegistrar().register(container.getBeansByAnnotation(Controller.class)))
                .build();

        server.start(8084);
        try {
            Thread.sleep(1000000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        server.stop();
    }
}

