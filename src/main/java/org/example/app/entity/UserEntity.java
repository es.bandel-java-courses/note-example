package org.example.app.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "users")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString

public class UserEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(nullable = false, columnDefinition = "TEXT", unique = true)
    private String login;
    @Column(nullable = false, columnDefinition = "TEXT")
    private String password;
    @Column(columnDefinition = "boolean default false")
    private boolean removed;
}
