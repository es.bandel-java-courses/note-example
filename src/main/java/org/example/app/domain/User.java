package org.example.app.domain;

import lombok.Builder;
import lombok.Value;
import lombok.With;

@Value
@With
@Builder
public class User {
    long id;
    String login;
    String passwordHash;
}
