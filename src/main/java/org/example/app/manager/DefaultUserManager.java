package org.example.app.manager;

import lombok.extern.slf4j.Slf4j;
import org.example.app.dto.GetUserByIdRS;
import org.example.app.dto.UserRegisterRQ;
import org.example.app.dto.UserRegisterRS;
import org.example.app.entity.UserEntity;
import org.example.app.exception.EasyPasswordException;
import org.example.app.exception.ForbiddenLoginException;
import org.example.app.exception.ItemNotFoundException;
import org.example.app.repository.UserRepository;
import org.example.framework.di.annotation.Component;
import org.example.framework.security.annotation.Audit;
import org.example.framework.security.auth.AuthenticationToken;
import org.example.framework.security.auth.LoginPasswordAuthenticationToken;
import org.example.framework.server.exception.UnsupportAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Slf4j
@Component
public class DefaultUserManager implements UserManager {
    private final PasswordEncoder encoder;
    private final UserRepository repo;
    private List<String> forbiddenLogins;
    private List<String> topPasswords;
    private boolean isRead = false;

    public DefaultUserManager(PasswordEncoder encoder, UserRepository repo) {
        this.encoder = encoder;
        this.repo = repo;
        try {
            forbiddenLogins = Files.newBufferedReader(Paths.get("forbiddenLogin.txt"))
                    .lines()
                    .collect(Collectors.toList());

            topPasswords = Files.newBufferedReader(Paths.get("topPassword.txt"))
                    .lines()
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    @Audit
    public UserRegisterRS create(final UserRegisterRQ requestDTO) {

        final String login = requestDTO.getLogin().trim().toLowerCase();
        final String password = requestDTO.getPassword();

        try {
            final boolean isForbiddenLogin = forbiddenLogins.stream().anyMatch(o -> o.equals(login));
            if (isForbiddenLogin) {
                log.error("this login is forbidden: {}", login);
                throw new ForbiddenLoginException("this login is forbidden");
            }
            final boolean isTopPassword = topPasswords.stream().anyMatch(o -> o.equals(password));
            if (isTopPassword) {
                log.error("your password is too easy: {}", password);
                throw new EasyPasswordException("password is too easy");
            }

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        final String encodedPassword = encoder.encode(password);
        UserEntity user = new UserEntity(0, login, encodedPassword, false);
        repo.save(user);
        return new UserRegisterRS(user.getLogin());
    }

    @Override
    public GetUserByIdRS getById(final long id) {
        final Optional<UserEntity> user;
        user = repo.getById(id);
        return user
                .map(o -> new GetUserByIdRS(o.getId(), o.getLogin()))
                .orElseThrow(ItemNotFoundException::new)
                ;
    }

    @Override
    public boolean authenticate(final AuthenticationToken request) {
        if (!(request instanceof LoginPasswordAuthenticationToken)) {
            throw new UnsupportAuthenticationToken();
        }
        final LoginPasswordAuthenticationToken converted = (LoginPasswordAuthenticationToken) request;
        final String login = converted.getLogin();
        final String password = (String) converted.getCredentials();
        final String encodedPassword;
        List<UserEntity> users = repo.getAll(0, 50);
        for (UserEntity user : users) {
            if (user.getLogin().equals(login) && encoder.matches(password, (user.getPassword()))) {
                return true;
            }
        }
        return false;
    }
}

