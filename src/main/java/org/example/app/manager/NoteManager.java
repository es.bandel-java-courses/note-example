package org.example.app.manager;

import org.example.app.dto.NoteRQ;
import org.example.app.dto.NoteRS;

import java.util.List;

public interface NoteManager {
    NoteRS getById(long id);

    List<NoteRS> getAll();

    NoteRS create(NoteRQ requestDTO);

    NoteRS removeById(long id);

    NoteRS updateById(long id, NoteRQ requestDTO);
}
