package org.example.app.manager;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.app.dto.NoteRQ;
import org.example.app.dto.NoteRS;
import org.example.app.entity.NoteEntity;
import org.example.app.entity.UserEntity;
import org.example.app.exception.ItemNotFoundException;
import org.example.app.repository.NoteRepository;
import org.example.app.repository.UserRepository;
import org.example.framework.di.annotation.Component;
import org.example.framework.security.annotation.HasRole;
import org.example.framework.security.auth.SecurityContext;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.Principal;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Component
public class DefaultNoteManager implements NoteManager {
    private final NoteRepository repoNote;
    private final UserRepository repoUser;

    @HasRole("ROLE_USER")
    @Override
    public NoteRS create(final NoteRQ requestDTO) {
        final Principal principal = SecurityContext.getPrincipal();
        String author = principal.getName();
        String content = requestDTO.getContent();
        Base64.Decoder decoder = Base64.getDecoder();
        byte[] media = decoder.decode(requestDTO.getMedia());
        UserEntity user = repoUser.getByLogin(author);
        NoteEntity note = new NoteEntity(0, content, media, user, false);
        repoNote.save(note);
        return new NoteRS(note.getContent(), note.getMedia(), note.isRemoved());
    }


    @HasRole("ROLE_USER")
    @Override
    public NoteRS getById(long id) {
        final Principal principal = SecurityContext.getPrincipal();
        final Optional<NoteEntity> note;
        note = repoNote.getById(id);
        String name = note.map(o -> o.getAuthor()
                        .getLogin())
                .orElseThrow(ItemNotFoundException::new);
        boolean removed = note.map(o -> o.isRemoved())
                .orElseThrow(ItemNotFoundException::new);
        if (removed == true) {
            throw new ItemNotFoundException("This note was deleted");
        }
        if (name.equals(principal.getName())) {
            return note
                    .map(o -> new NoteRS(o.getContent(), o.getMedia(), o.isRemoved()))
                    .orElseThrow(ItemNotFoundException::new)
                    ;
        } else {
            throw new ItemNotFoundException("You are not author");
        }
    }

    @HasRole("ROLE_USER")
    @Override
    public List<NoteRS> getAll() {
        final Principal principal = SecurityContext.getPrincipal();
        final List<NoteEntity> notes;
        notes = repoNote.getAll(0, 50);
        if (!notes.isEmpty()) {
            List<NoteRS> notesOfCurrentUser = notes.
                    stream().filter(note -> note.getAuthor().getLogin().equals(principal.getName()))
                    .map(o -> new NoteRS(o.getContent(), o.getMedia(), o.isRemoved()))
                    .collect(Collectors.toList());
            return notesOfCurrentUser;
        } else {
            throw new ItemNotFoundException("There are no notes of this author!");
        }

    }

    @HasRole("ROLE_USER")
    @Override
    public NoteRS removeById(long id) {
        final Principal principal = SecurityContext.getPrincipal();
        final Optional<NoteEntity> note;
        note = repoNote.getById(id);
        String name = note.map(o -> o.getAuthor()
                        .getLogin())
                .orElseThrow(ItemNotFoundException::new);
        boolean removed = note.map(o -> o.isRemoved())
                .orElseThrow(ItemNotFoundException::new);
        if (removed == true) {
            throw new ItemNotFoundException("This note was deleted");
        }
        if (name.equals(principal.getName())) {
            final NoteEntity noteEntity = note.get();
            noteEntity.setRemoved(true);
            repoNote.save(noteEntity);
            return new NoteRS(noteEntity.getContent(), noteEntity.getMedia(), noteEntity.isRemoved());
        } else {
            throw new ItemNotFoundException("You are not author");
        }
    }

    @HasRole("ROLE_USER")
    @Override
    public NoteRS updateById(long id, final NoteRQ requestDTO) {
        final Principal principal = SecurityContext.getPrincipal();
        final Optional<NoteEntity> note;
        String content = requestDTO.getContent();
        note = repoNote.getById(id);
        String name = note.map(o -> o.getAuthor()
                        .getLogin())
                .orElseThrow(ItemNotFoundException::new);
        boolean removed = note.map(o -> o.isRemoved())
                .orElseThrow(ItemNotFoundException::new);
        if (removed == true) {
            throw new ItemNotFoundException("This note was deleted");
        }
        if (name.equals(principal.getName())) {
            final NoteEntity noteEntity = note.get();
            noteEntity.setContent(content);
            Base64.Decoder decoder = Base64.getDecoder();
            byte[] media = decoder.decode(requestDTO.getMedia());
            noteEntity.setMedia(media);
            repoNote.save(noteEntity);
            return new NoteRS(noteEntity.getContent(), noteEntity.getMedia(), noteEntity.isRemoved());
        } else {
            throw new ItemNotFoundException("You are not author");
        }
    }

}
