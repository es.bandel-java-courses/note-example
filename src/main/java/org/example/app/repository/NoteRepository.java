package org.example.app.repository;

import org.example.app.entity.NoteEntity;
import org.example.repo.framework.CrudRepository;

public interface NoteRepository extends CrudRepository<NoteEntity, Long> {
}
