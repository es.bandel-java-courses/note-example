package org.example.app.handler;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.app.dto.GetUserByIdRS;
import org.example.app.dto.UserRegisterRQ;
import org.example.app.dto.UserRegisterRS;
import org.example.app.manager.UserManager;
import org.example.framework.di.annotation.Autowired;
import org.example.framework.di.annotation.Component;
import org.example.framework.server.annotation.*;
import org.example.framework.server.http.HttpMethods;

@Slf4j
@RequiredArgsConstructor
@Component
@Controller
@Autowired
public class UserController {
    private final UserManager manager;

    @RequestMapping(method = HttpMethods.GET, path = "^/users/(?<id>\\d+)$")
    @ResponseBody
    public GetUserByIdRS getById(@PathVariable("id") final long id) {
        return manager.getById(id);
    }

    @RequestMapping(method = HttpMethods.POST, path = "^/regist$")
    @ResponseBody
    public UserRegisterRS register(@RequestBody final UserRegisterRQ requestDTO) {
        return manager.create(requestDTO);
    }
}
