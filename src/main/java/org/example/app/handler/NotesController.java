package org.example.app.handler;


import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.app.dto.NoteRQ;
import org.example.app.dto.NoteRS;
import org.example.app.manager.NoteManager;
import org.example.framework.di.annotation.Autowired;
import org.example.framework.di.annotation.Component;
import org.example.framework.server.annotation.*;
import org.example.framework.server.http.HttpMethods;

import java.io.IOException;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Component
@Controller
@Autowired
public class NotesController {
    private final Gson gson;
    private final NoteManager manager;

    @RequestMapping(method = HttpMethods.POST, path = "^/notes$")
    @ResponseBody
    public NoteRS create(@RequestBody final NoteRQ requestDTO) throws IOException {
        return manager.create(requestDTO);
    }

    @RequestMapping(method = HttpMethods.GET, path = "^/notes/(?<id>\\d+)$")
    @ResponseBody
    public NoteRS getById(@PathVariable("id") final long id) {
        return manager.getById(id);
    }

    @RequestMapping(method = HttpMethods.DELETE, path = "^/notes/(?<id>\\d+)$")
    @ResponseBody
    public NoteRS removeById(@PathVariable("id") final long id) {
        return manager.removeById(id);
    }

    @RequestMapping(method = HttpMethods.GET, path = "^/notes$")
    @ResponseBody
    public List<NoteRS> getAll() throws IOException {
        return manager.getAll();
    }

    @RequestMapping(method = HttpMethods.PUT, path = "^/notes/(?<id>\\d+)$")
    @ResponseBody
    public NoteRS updateById(@PathVariable("id") final long id, @RequestBody final NoteRQ requestDTO) {
        return manager.updateById(id, requestDTO);
    }
}
