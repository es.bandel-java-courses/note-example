FROM maven:3-openjdk-8-slim AS build
WORKDIR /app/build
COPY . .
COPY src/main/resources/META-INF/persistence.docker.xml src/main/resources/META-INF/persistence.xml
RUN mvn -B package

FROM openjdk:8-slim
WORKDIR /app/bin
COPY --from=build /app/build/target/app.jar .
CMD ["java", "-jar", "app.jar"]